var array = []
var contador=0; 

function Adicionar(){
    var nota = document.querySelector("#nota").value
    if (!nota || isNaN(nota)){
        window.alert('Insira um Número!')
    } else if (nota < 0 || nota > 10) {
        window.alert('Insira um Número Válido!')
    } else {
        array.push(nota);
        contador++;
        var paragrafo = document.getElementById("lista_notas").innerHTML;        
        paragrafo += `A nota ${contador} é ${nota}\n`
  
        document.getElementById("lista_notas").innerHTML = paragrafo;

    }
    document.querySelector('#nota').value = '';
}

function Media(){
    let soma=0;
    for (let i=0; i<array.length; i++){
        soma += parseFloat(array[i])
    }
    var m = document.querySelector('#resultado').innerHTML;
    m = `A media: ${soma/array.length}.`;
    document.querySelector('#resultado').innerHTML = m;
    console.log(`Media: ${soma/array.length}`)
}
